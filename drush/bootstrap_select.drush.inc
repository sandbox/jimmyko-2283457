<?php

/**
 * @file
 * drush integration for bootstrap_select.module.
 */

/**
 * Directory name for Bootstrap select.
 */
define('BOOTSTRAP_SELECT_FOLDER_NAME', 'bootstrap_select');

/**
 * Download URL.
 */
define('BOOTSTRAP_SELECT_DOWNLOAD_URL', 'https://github.com/silviomoreto/bootstrap-select/archive/master.zip');

/**
 * Implements hook_drush_command().
 */
function bootstrap_select_drush_command() {
  $items = array();

  $items['bootstrap-select-download'] = array(
    'callback' => 'bootstrap_select_drush_download',
    'description' => dt('Downloads the Bootstrap select.'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'options' => array(
      'path' => dt('Optional. The path to your shared libraries. If omitted Drush will use the default location.'),
    ),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function bootstrap_select_drush_help($section) {
  switch ($section) {
    case 'drush:bootstrap-select-download':
      $path = 'sites'.DIRECTORY_SEPARATOR.'all'.DIRECTOR_SEPARATOR.'libraries';
      $msg = dt("Downloads Bootstrap select. Default location is @path.", array('@path' => $path));
      return $msg;
  }
}

/**
 * Download callback.
 */
function bootstrap_select_drush_download() {
  if (!drush_shell_exec('type unzip')) {
    return drush_set_error(dt('Missing dependency: unzip. Install it before using this command.'));
  }

  $download_url = BOOTSTRAP_SELECT_DOWNLOAD_URL;

  if (!$path = drush_get_option('path')) {
    $path = 'sites'.DIRECTORY_SEPARATOR.'all'.DIRECTORY_SEPARATOR.'libraries';
  }

  // Create path directory if needed.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory #@path was created', array('@path' => $path)), 'notice');
  }

  // Create the folder if it does not exist.
  $lib_path .= $path . DIRECTORY_SEPARATOR . BOOTSTRAP_SELECT_FOLDER_NAME;
  if (is_dir($lib_path)) {
    return drush_set_error(dt('Directory @path exists. Remove it and try again.', array('@path' => $lib_path)));
  }

  // Set the directory to the download location.
  $old_dir = getcwd();
  $tmp = sys_get_temp_dir();
  chdir($tmp);

  // Remove file and directory with same name in tmp.
  $tmp_file = $tmp.DIRECTORY_SEPARATOR.'master.zip';
  drush_op('unlink', $tmp_file);
  $tmp_dir  = $tmp.DIRECTORY_SEPARATOR.'bootstrap-select-master';
  if (is_dir($tmp_dir)) {
    _bootstrap_select_drush_recursive_rmdir($tmp_dir);
  } else {
    drush_op('unlink', $tmp_dir);
  }

  // Download the zip file.
  if (!drush_shell_exec('wget ' . $download_url)) {
    drush_shell_exec('curl -O ' . $download_url);
  }
  if (is_file($tmp_file)) {
    drush_shell_exec('unzip '.$tmp_file);
  }
  drush_op('rename', $tmp_dir, $old_dir.DIRECTORY_SEPARATOR.$lib_path);

  // Set working directory  back to the previous working directory.
  chdir($old_dir);

  if (is_dir($lib_path)) {
    drush_log(dt('Bootstrap select has been downloaded to @path', array('@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to download Bootstrap select to @path', array('@path' => $path)), 'error');
  }
}

function _bootstrap_select_drush_recursive_rmdir($dir) {
  $it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
  $files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
  foreach ($files as $file) {
    if ($file->getFilename() === '.' || $file->getFilename() === '..') {
      continue;
    }
    if ($file->isDir()) {
      drush_op('rmdir', $file->getRealPath());
    } else {
      drush_op('unlink', $file->getRealPath());
    }
  }
}
